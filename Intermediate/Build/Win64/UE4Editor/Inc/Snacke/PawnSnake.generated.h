// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNACKE_PawnSnake_generated_h
#error "PawnSnake.generated.h already included, missing '#pragma once' in PawnSnake.h"
#endif
#define SNACKE_PawnSnake_generated_h

#define Snacke_Source_Snacke_PawnSnake_h_15_SPARSE_DATA
#define Snacke_Source_Snacke_PawnSnake_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Snacke_Source_Snacke_PawnSnake_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Snacke_Source_Snacke_PawnSnake_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPawnSnake(); \
	friend struct Z_Construct_UClass_APawnSnake_Statics; \
public: \
	DECLARE_CLASS(APawnSnake, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snacke"), NO_API) \
	DECLARE_SERIALIZER(APawnSnake)


#define Snacke_Source_Snacke_PawnSnake_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPawnSnake(); \
	friend struct Z_Construct_UClass_APawnSnake_Statics; \
public: \
	DECLARE_CLASS(APawnSnake, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snacke"), NO_API) \
	DECLARE_SERIALIZER(APawnSnake)


#define Snacke_Source_Snacke_PawnSnake_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APawnSnake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APawnSnake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnSnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnSnake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnSnake(APawnSnake&&); \
	NO_API APawnSnake(const APawnSnake&); \
public:


#define Snacke_Source_Snacke_PawnSnake_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnSnake(APawnSnake&&); \
	NO_API APawnSnake(const APawnSnake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnSnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnSnake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APawnSnake)


#define Snacke_Source_Snacke_PawnSnake_h_15_PRIVATE_PROPERTY_OFFSET
#define Snacke_Source_Snacke_PawnSnake_h_12_PROLOG
#define Snacke_Source_Snacke_PawnSnake_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snacke_Source_Snacke_PawnSnake_h_15_PRIVATE_PROPERTY_OFFSET \
	Snacke_Source_Snacke_PawnSnake_h_15_SPARSE_DATA \
	Snacke_Source_Snacke_PawnSnake_h_15_RPC_WRAPPERS \
	Snacke_Source_Snacke_PawnSnake_h_15_INCLASS \
	Snacke_Source_Snacke_PawnSnake_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snacke_Source_Snacke_PawnSnake_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snacke_Source_Snacke_PawnSnake_h_15_PRIVATE_PROPERTY_OFFSET \
	Snacke_Source_Snacke_PawnSnake_h_15_SPARSE_DATA \
	Snacke_Source_Snacke_PawnSnake_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snacke_Source_Snacke_PawnSnake_h_15_INCLASS_NO_PURE_DECLS \
	Snacke_Source_Snacke_PawnSnake_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNACKE_API UClass* StaticClass<class APawnSnake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snacke_Source_Snacke_PawnSnake_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
