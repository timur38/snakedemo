// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNACKE_SnackeGameModeBase_generated_h
#error "SnackeGameModeBase.generated.h already included, missing '#pragma once' in SnackeGameModeBase.h"
#endif
#define SNACKE_SnackeGameModeBase_generated_h

#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_SPARSE_DATA
#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_RPC_WRAPPERS
#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnackeGameModeBase(); \
	friend struct Z_Construct_UClass_ASnackeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnackeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snacke"), NO_API) \
	DECLARE_SERIALIZER(ASnackeGameModeBase)


#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnackeGameModeBase(); \
	friend struct Z_Construct_UClass_ASnackeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnackeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snacke"), NO_API) \
	DECLARE_SERIALIZER(ASnackeGameModeBase)


#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnackeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnackeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnackeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnackeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnackeGameModeBase(ASnackeGameModeBase&&); \
	NO_API ASnackeGameModeBase(const ASnackeGameModeBase&); \
public:


#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnackeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnackeGameModeBase(ASnackeGameModeBase&&); \
	NO_API ASnackeGameModeBase(const ASnackeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnackeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnackeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnackeGameModeBase)


#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snacke_Source_Snacke_SnackeGameModeBase_h_12_PROLOG
#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_SPARSE_DATA \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_RPC_WRAPPERS \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_INCLASS \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snacke_Source_Snacke_SnackeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_SPARSE_DATA \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Snacke_Source_Snacke_SnackeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNACKE_API UClass* StaticClass<class ASnackeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snacke_Source_Snacke_SnackeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
