// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnackeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNACKE_API ASnackeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
